
public enum Tile {
	
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");
	
	//field
	private String name;
	
	//constructor
	private Tile (String name) {
		this.name = name;
	}
	
	//getter
	public String getName() {
		return name;
	}
	
}