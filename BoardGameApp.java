import java.util.Scanner;

public class BoardGameApp {
	public static void main (String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		
		System.out.println("*** Welcome to the Game! ***");
		Board board = new Board();
		int numCastles = 7;
		int turns = 0;
		
		//loops through the whole game
		while (numCastles > 0 && turns < 8)
		{
			System.out.println("Number of Castles: " + numCastles);
			System.out.println("Number of Turns: " + turns);
			System.out.println(board.toString());
			
			System.out.println("Enter an row number (0-4)");
			int row = reader.nextInt();
			System.out.println("Enter an column number (0-4)");
			int col = reader.nextInt();
			
			int tileStatus = board.placeToken(row, col);
			
			//if result is negative, will loop again
			while (tileStatus < 0) {
				System.out.println("** Tile occupied or invalid number, enter again **");
				System.out.println("Enter an row number (0-4)");
				row = reader.nextInt();
				System.out.println("Enter an column number (0-4)");
				col = reader.nextInt();
				tileStatus = board.placeToken(row, col);
			}
			
			if (tileStatus == 1) {
				System.out.println("** There's a hidden wall here **");
			}
			else {
				System.out.println("Sucessfully placed a castle");
				numCastles--;
			}
			
			turns++;
		}
		
		//displays the final board and message
		System.out.println("------------------");
		System.out.println(board.toString());
		
		if (numCastles == 0) 
		{
			System.out.println("You won!");
		
		} else {
			System.out.println("You lost the game, try again");
		}
	}
}