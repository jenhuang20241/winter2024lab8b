import java.util.Random;


public class Board {
	
	//field
	private Tile[][] grid;
	private final int SIZE = 5;
	
	//constructor
	public Board () 
	{
		this.grid = new Tile[SIZE][SIZE];
		Random rng = new Random();
		
		//initialize random tiles to HIDDEN_WALL and rest to BLANK
		for (int i=0; i<this.grid.length; i++) 
		{
			int randIndex = rng.nextInt(grid[i].length);
			
			for (int j=0; j<this.grid[i].length; j++)
			{
				if (j == randIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				
				} else {
					this.grid[i][j] = Tile.BLANK;
				}
			}
		}
	}

	//toString
	public String toString()
	{
		String result = "";
		
		for (int i=0; i<this.grid.length; i++) 
		{
			for (int j=0; j<this.grid[i].length; j++)
			{
				result += grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	
	//custom method placeToken
	public int placeToken(int row, int col)
	{
		int result;
		
		if (row > grid.length-1 || row < 0 || 
			col > grid.length-1 || col < 0 ) {
			result = -2;
		}
		else if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			result = -1;
		}
		else if (grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			result = 1;
		}
		else {
			grid[row][col] = Tile.CASTLE;
			result = 0;
		}
		return result;
	}
}